/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Switch.hpp"

DesQUI::Switch::Switch( QWidget *parent ) : QAbstractButton( parent ) {
    mAnim = new QVariantAnimation( this );

    // Makes no sense if we have this smaller than 32 px
    setMinimumSize( mSize );

    // Let's init the widget with a sane value.
    resize( mSize );

    // Yes, this button is checkeable.
    setCheckable( true );

    // Repaint to make the thumb move
    connect(
        mAnim, &QVariantAnimation::valueChanged, [ = ] ( QVariant var ) {
            mThumbPos = var.toInt();
            repaint();
        }
    );

    // Trigger the animation when clicked
    connect(
        this, &QAbstractButton::clicked, [ this ] ( bool checked ) {
            if ( checked ) {
                mAnim->setStartValue( 1 );
                mAnim->setEndValue( width() - (width() / 2 - 2) - 1 );
                mAnim->setDuration( 120 );
                mAnim->start();
            }

            else {
                mAnim->setStartValue( width() - (width() / 2 - 2) - 1 );
                mAnim->setEndValue( 1 );
                mAnim->setDuration( 120 );
                mAnim->start();
            }
        }
    );
}


DesQUI::Switch::~Switch() {
    delete mAnim;
}


void DesQUI::Switch::paintEvent( QPaintEvent *e ) {
    QPainter p( this );

    p.setRenderHint( QPainter::Antialiasing, true );

    p.setPen( Qt::NoPen );

    if ( isEnabled() ) {
        // Track
        p.setBrush( isChecked() ? palette().color( QPalette::Highlight ) : Qt::gray );
        p.setOpacity( isChecked() ? 0.5 : 0.38 );
        p.drawRoundedRect( QRect( 0, height() / 4, width(), height() / 2 ), height() / 4, height() / 4 );

        // Thumb
        p.setOpacity( 1.0 );
        p.setBrush( QColor( 255, 255, 255, 150 ) );
        p.drawEllipse( QRectF( mThumbPos, height() / 4 + 1, height() / 2 - 2, height() / 2 - 2 ) );
    }

    else {
        // Track
        p.setBrush( Qt::black );
        p.setOpacity( 0.12 );
        p.drawRoundedRect( QRect( 0, height() / 4, width(), height() / 2 ), height() / 4, height() / 4 );

        // Thumb
        p.setOpacity( 1.0 );
        p.setBrush( Qt::lightGray );
        p.drawEllipse( QRectF( mThumbPos, height() / 4 + 1, height() / 2 - 2, height() / 2 - 2 ) );
    }

    p.end();

    e->accept();
}


void DesQUI::Switch::enterEvent( QMouseEnterEvent *e ) {
    setCursor( Qt::PointingHandCursor );
    QAbstractButton::enterEvent( e );
}


QSize DesQUI::Switch::sizeHint() const {
    return mSize;
}
