/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include "desqui/CircularProgress.hpp"

static QList<QColor> colors = {
    0x1ABC9C, 0x9B59B6, 0xFF8F00, 0xE74C3C, 0x4E342E,
    0x8E44AD, 0xEF6C00, 0xC0392B, 0x34495E, 0x424242,
    0x5499C7, 0xCD6155, 0xF5B041, 0x566573, 0x58D68D
};

DesQUI::CircularProgress::CircularProgress( QWidget *parent ) : QWidget( parent ) {
    fm = new QFontMetrics( font() );

    mRadius    = 50;
    mMinRadius = 50;
    labelWidth = fm->horizontalAdvance( "MMMMMMM" );
}


QSize DesQUI::CircularProgress::sizeHint() const {
    /*
     * Width = 5 + mRadius * 2 + 5 + labelWidth + 5
     * Height = 5 + mRadius * 2 + 5
     */
    return QSize( mRadius * 2 + labelWidth + 15, mRadius * 2 + 10 );
}


QSize DesQUI::CircularProgress::minimumSizeHint() {
    /*
     * Width = 5 + mMinRadius * 2 + 5 + labelWidth + 5
     * Height = 5 + mMinRadius * 2 + 5
     */
    return QSize( mMinRadius * 2 + labelWidth + 15, mMinRadius * 2 + 10 );
}


int DesQUI::CircularProgress::minimumWidth() {
    return minimumSizeHint().width();
}


int DesQUI::CircularProgress::minimumHeight() {
    return minimumSizeHint().height();
}


void DesQUI::CircularProgress::setRadius( int radius ) {
    /* Fixed radius; ignore resize */
    if ( mMaxRadius == mMinRadius ) {
        mRadius = mMaxRadius;
        return;
    }

    mRadius = radius;
    QWidget::resize( QSize( radius * 2 + labelWidth + 15, radius * 2 + 10 ) );

    repaint();
}


void DesQUI::CircularProgress::setFixedRadius( int radius ) {
    mMaxRadius = radius;
    mMinRadius = radius;
    mRadius    = radius;

    QWidget::setFixedSize( QSize( radius * 2 + labelWidth + 15, radius * 2 + 10 ) );
    repaint();
}


void DesQUI::CircularProgress::setMinimumRadius( int radius ) {
    mMinRadius = radius;

    if ( mRadius < radius ) {
        setRadius( radius );
    }

    QWidget::setMinimumSize( QSize( mMinRadius * 2 + labelWidth + 15, mMinRadius * 2 + 10 ) );

    repaint();
}


void DesQUI::CircularProgress::setMaximumRadius( int radius ) {
    mMaxRadius = radius;
    QWidget::setMaximumSize( QSize( mMaxRadius * 2 + labelWidth + 15, mMaxRadius * 2 + 10 ) );

    repaint();
}


QSize DesQUI::CircularProgress::size() const {
    /*
     * Width = 5 + mRadius * 2 + 5 + labelWidth + 5
     * Height = 5 + mRadius * 2 + 5
     */
    return QSize( mRadius * 2 + labelWidth + 15, mRadius * 2 + 10 );
}


void DesQUI::CircularProgress::resize( int w, int h ) {
    resize( QSize( w, h ) );
}


void DesQUI::CircularProgress::resize( const QSize& size ) {
    int h = size.height();
    int w = size.width();

    /* Landscape: radius = (height - 10)/2 */
    if ( h < w ) {
        if ( (h - 10) < 100 ) {
            setRadius( 50 );
        }

        else {
            setRadius( (h - 10) / 2 );
        }
    }

    /* Portrait Mode: Make it landscape */
    else {
        if ( (w - 15 - labelWidth) < 100 ) {
            setRadius( 50 );
        }

        else {
            setRadius( (w - 15 - labelWidth) / 2 );
        }
    }

    repaint();
}


void DesQUI::CircularProgress::setFont( const QFont& font ) {
    fm         = new QFontMetrics( font );
    labelWidth = fm->horizontalAdvance( "MMMMMMM" );

    QWidget::setFont( font );
    repaint();
}


void DesQUI::CircularProgress::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );
    painter.fillRect( rect(), QColor( 0, 0, 0, 10 ) );
    painter.fillRect( rect(), QColor( 255, 255, 255, 10 ) );

    painter.end();

    pEvent->accept();
}
