/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include "desqui/UsagePie.hpp"

static QList<QColor> colors = {
    0x2ECC71, 0xD35400, 0x3498DB, 0xF1C40F, 0xE67E22,
    0x1ABC9C, 0x9B59B6, 0x34495E, 0xE74C3C, 0xC0392B,
    0x8E44AD, 0xFF8F00, 0xEF6C00, 0x4E342E, 0x424242,
    0x5499C7, 0x58D68D, 0xCD6155, 0xF5B041, 0x566573
};

DesQUI::UsagePie::UsagePie( QString resource, int segments, QWidget *parent ) : DesQUI::CircularProgress( parent ) {
    mSegments = segments;
    mUnits    = DesQUI::UsagePie::PERCENT;
    resName   = QString( resource );

    setRadius( 50 );

    setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );

    for ( int i = 0; i < segments; i++ ) {
        mValues.push_back( PieResourceValue { 2, 1 } );
    }
}


void DesQUI::UsagePie::setUnits( DesQUI::UsagePie::Units units ) {
    mUnits = units;
    repaint();
}


void DesQUI::UsagePie::setLabels( QStringList labels ) {
    mLabels.clear();
    mLabels << labels;

    QFontMetrics fm( font() );

    labelHeight = fm.lineSpacing() * labels.count() + (labels.count() - 1) * 5;

    repaint();
}


void DesQUI::UsagePie::setValues( PieResourceValues values ) {
    mValues.clear();

    qint64 max = 0;

    mMax = 0;
    for ( PieResourceValue val: values ) {
        mValues.push_back( val );
        mMax += val.total;

        if ( val.total > max ) {
            max = val.total;
        }
    }

    if ( max >= DesQ::Utils::SizeHint::TiB ) {
        sizeFormatHint = DesQ::Utils::SizeHint::TiB;
    }
    else if ( max >= DesQ::Utils::SizeHint::GiB ) {
        sizeFormatHint = DesQ::Utils::SizeHint::GiB;
    }
    else if ( max >= DesQ::Utils::SizeHint::MiB ) {
        sizeFormatHint = DesQ::Utils::SizeHint::MiB;
    }
    else if ( max >= DesQ::Utils::SizeHint::KiB ) {
        sizeFormatHint = DesQ::Utils::SizeHint::KiB;
    }
    else {
        sizeFormatHint = DesQ::Utils::SizeHint::Auto;
    }

    repaint();
}


void DesQUI::UsagePie::paintEvent( QPaintEvent *pEvent ) {
    /* Height will always be radius + 10 */
    int    side = mRadius * 2;
    QImage img( side, side, QImage::Format_ARGB32 );

    img.fill( Qt::transparent );

    QPainter painter( &img );

    painter.setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing );

    painter.save();
    QPalette pltt = qApp->palette();
    QColor   bg   = pltt.color( QPalette::Highlight );

    bg.setAlpha( 27 );
    painter.setPen( Qt::NoPen );
    painter.setBrush( bg );
    painter.drawEllipse( QPointF( mRadius, mRadius ), mRadius - 1, mRadius - 1 );
    painter.restore();

    /* Start at 12 o'clock */
    double startAngle = 90.0 * 16.0;

    for ( int s = 0; s < mSegments; s++ ) {
        painter.save();

        painter.setPen( Qt::NoPen );
        painter.setBrush( colors.at( s ) );

        double spanAngle = -360.0 * 16 * mValues.at( s ).total / mMax;

        double x = 3.0;
        double y = 3.0;

        painter.drawPie( QRectF( x, y, side - 2 * x, side - 2 * y ), startAngle, spanAngle );

        painter.setBrush( colors.at( s ).darker( 120 ) );

        srand( time( 0 ) );
        double usage = 1.0 * mValues.at( s ).used / mValues.at( s ).total;
        painter.drawPie( QRectF( x, y, side - 2 * x, side - 2 * y ), startAngle, spanAngle * usage );

        painter.restore();

        startAngle += spanAngle;
    }

    QString       text;
    QList<qint64> totals;

    for ( PieResourceValue val: mValues ) {
        totals << val.total;
    }
    switch ( mUnits ) {
        case DesQUI::CircularProgress::PERCENT: {
            text = "%";
            break;
        }

        case DesQUI::CircularProgress::SIZE: {
            text = DesQ::Utils::formatSizeStr( 0, sizeFormatHint );
            break;
        }

        case DesQUI::CircularProgress::SPEED: {
            text = DesQ::Utils::formatSizeStr( 0, sizeFormatHint ) + "/s";
            break;
        }
    }

    painter.setPen( Qt::NoPen );
    painter.setBrush( pltt.color( QPalette::HighlightedText ).darker() );
    painter.drawEllipse( QPoint( mRadius, mRadius ), 25, 25 );

    painter.save();
    painter.setPen( pltt.color( QPalette::HighlightedText ) );
    painter.setFont( QFont( font().family(), 8 ) );
    painter.drawText( QRectF( 0, 0, side, side ), Qt::AlignCenter, resName + "\n" + text );
    painter.restore();

    painter.end();

    painter.begin( this );
    painter.setRenderHints( QPainter::Antialiasing );

    /* Background rectangle */
    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( QColor( 0, 0, 0, 10 ) );
    painter.drawRoundedRect( rect(), 3, 3 );
    painter.setBrush( QColor( 255, 255, 255, 10 ) );
    painter.drawRoundedRect( rect(), 3, 3 );
    painter.restore();

    /* Resource */
    painter.drawImage( QRect( 5, (height() - side) / 2, side, side ), img );

    /* Labels */
    painter.save();
    int y = (height() - labelHeight) / 2 - 5;           // We will be adding 5, so reduce that amount at

    // first.

    y = (y > 0 ? y : 0);
    QRectF bound( 0, y, 0, 0 );

    for ( int s = 0; s < mLabels.count(); s++ ) {
        painter.setPen( colors.value( s ) );
        painter.drawText( QRect( side + 10, bound.bottom() + 5, labelWidth, height() ), Qt::AlignTop | Qt::AlignRight, mLabels.value( s ), &bound );
    }
    painter.restore();

    painter.end();

    pEvent->accept();
}
