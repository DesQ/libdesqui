/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QString>
#include <QStringList>
#include <QWidget>

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    #define QMouseEnterEvent    QEvent
#else
    #define QMouseEnterEvent    QEnterEvent
#endif

namespace DesQ {
    namespace Plugin {
        class PanelInterface;
    }

    namespace Panel {
        class PluginWidget;
        class PluginPopupWidget;
    }
}

class DesQ::Plugin::PanelInterface {
    public:
        explicit PanelInterface();
        virtual ~PanelInterface();

        /* Name of the plugin */
        virtual QString name() = 0;

        /* Icon for the plugin */
        virtual QIcon icon() = 0;

        /* The plugin version */
        virtual QString version() = 0;

        /* The QWidget */
        virtual DesQ::Panel::PluginWidget *widget( QWidget *parent ) = 0;
};


/**
 * This is a special class that defines a widget for the panel.
 * This widget can, technically, be of any size, though a size larger
 * than the screen resolution may be useless.
 * This class further defines a few sginals like clicked(), pressed()
 * released(), scrolled( int ), etc. These may be used appropriately
 * to provide various actions.
 * When the panel plugin emits showPopup(...) or showTooltip(...), the
 * widget contained in the signal will be shown as a layer-surface via
 * the layer-shell protocol. The plugin widget may choose when these
 * signal are emitted.
 * NOTE:
 * 1. The height of this widget should never exceed the panel height.
 *    The panel will not be resized, but this widget will be resized
 *    to fit the panel height.
 * 2. The user can change the panel size, develop your widgets so that
 *    they can dynamically adjust to the height.
 */
class DesQ::Panel::PluginWidget : public QWidget {
    Q_OBJECT;

    public:
        PluginWidget( QWidget *parent );
        virtual ~PluginWidget();

        /**
         * This function will be called automatically by DesQ Panel.
         * The user need not call this function.
         */
        void setPanelHeight( int );

        /**
         * This function will be called automatically by DesQ Panel.
         * The user need not call this function.
         */
        void setPanelScreen( QScreen * );

    private:
        bool mPressed = false;
        bool mInside  = false;

    protected:

        /**
         * Emit a signal when  the widget is pressed with mouse left button.
         * Remember to call DesQ::Panel::PluginWidget::mousePressEvent(...)
         * instead of QWidget::mousePressEvent(...) if you over-ride this,
         * and still want to receive pressed() signal.
         */
        void mousePressEvent( QMouseEvent * ) override;

        /**
         * Emit a signal when mouse left button press was released.
         * Remember to call DesQ::Panel::PluginWidget::mouseReleaseEvent(...)
         * instead of QWidget::mouseReleaseEvent(...) if you over-ride this,
         * and still want to receive released()/clicked() signals.
         */
        void mouseReleaseEvent( QMouseEvent * ) override;

        /**
         * The widget recieved a scroll event.
         * Remember to call DesQ::Panel::PluginWidget::wheelEvent(...)
         * instead of QWidget::wheelEvent(...) if you over-ride this,
         * and still want to receive scrolled(...) signal.
         */
        void wheelEvent( QWheelEvent * ) override;

        /** Mouse entered this widget */
        void enterEvent( QMouseEnterEvent * ) override;

        /** The mouse left this widget */
        void leaveEvent( QEvent * ) override;

        /** Show a mild backlight when mouse enters this widget */
        void paintEvent( QPaintEvent *pEvent ) override;

        int panelHeight = 36;

        QScreen *mScreen = nullptr;

    Q_SIGNALS:
        /** This widget was clicked (pressed + released) */
        void clicked();

        /** This widget was pressed */
        void pressed();

        /** This widget was released */
        void released();

        /** User scrolled on this widget */
        void scrolled( QPoint& );

        /** This widget was entered */
        void entered();

        /** This widget was exited */
        void exited();

        /** Signal emitted when panel thickness is changed */
        void panelThicknessChanged( int );

        /** Signal emitted when this panel's screen is changed */
        void panelScreenChanged( QScreen * );

        /**
         * The popup widget that will be shown via wlr-layer-shell protocol.
         * The plugin can decide when this popup has to be shown. i.e, on click
         * or on hover, etc. The contents of the popup may be updated dynamically,
         * just like any other widget. Attempting to show a new popup while already
         * displaying another one, will cause the existing popup to be closed.
         */
        void showPopup( QWidget * );
        void hidePopup();

        /**
         * The tooltip widget that will be shown via wlr-layer-shell protocol.
         * The plugin can decide when this popup has to be shown: typically on hover.
         * The contents of the tooltip may be updated dynamically, just like any
         * other widget. Attempting to show a new tooltip while already displaying
         * another one, will cause the existing tooltip to be closed.
         */
        void showTooltip( QWidget * );
        void hideTooltip();
};


/**
 * This is a special class that defines a popup widget for the panel.
 * This widget can, technically, be of any size, though a size larger
 * than the screen resolution may be useless.
 * This plugin is designed to autohide when it loses focus.
 */
class DesQ::Panel::PluginPopupWidget : public QWidget {
    Q_OBJECT;

    public:
        PluginPopupWidget( QWidget *parent );
        virtual ~PluginPopupWidget();

        // Q_SLOT void show() override;
        // Q_SLOT void hide() override;
        // Q_SLOT void close() override;

        Q_SIGNAL void hidePopup();

    protected:

        /** Fraw a border and background for this widget */
        void paintEvent( QPaintEvent *pEvent ) override;

        /** Hide this widget when it loses focus */
        void focusOutEvent( QFocusEvent *fEvent ) override;

        /** Hide this widget when Esc is pressed */
        void keyReleaseEvent( QKeyEvent *kEvent ) override;
};

Q_DECLARE_INTERFACE( DesQ::Plugin::PanelInterface, "org.DesQ.Plugin.Panel" );
