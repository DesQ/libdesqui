/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

namespace DesQUI {
    class Container;
    class LayoutManager;
    class WidgetFactory;
}

/**
 * Abstract Widget Factory.
 * This class is responsible for creating widgets and applying various properties.
 */
class DesQUI::WidgetFactory {
    public:
        WidgetFactory( QSize scrSize );
        virtual ~WidgetFactory() = default;

        /**
         * Create a widget wirth name @widget of type @type.
         * Once the widget is created, apply the properties specified in @props.
         * All widget with sizes set relative to screen size (0.0 - 1.0), will take a fixed size.
         */
        virtual QWidget * createWidget( QString widget, QString type, QVariantMap props );

        /** Names of the available widgets. The names need not be Qt class names */
        virtual QStringList availableWidgets();

        /** Types available for the given widget. */
        virtual QStringList availableTypes( QString widget );

        /** Properties that can be set for the given widget of the given type */
        virtual QStringList availableProperties( QString widget, QString type );

    protected:
        QSize mRefSize;
};


/**
 * Container widget
 * This class provides the container widget with suitable background color
 */
class DesQUI::Container : public QWidget {
    Q_OBJECT;

    public:
        Container( QVariantMap props, QString name );

    private:
        QColor bgColor;
        QColor borderColor;
        qreal borderRadius;

    protected:
        void paintEvent( QPaintEvent * );
};


/**
 * The class that reads the layout file and creates the widgets (using DesQUI::AbstractWidgetFactory
 * subclass).
 * Once the widgets are created, they are then laid out using QBoxLayout. This class can be used to create
 * both
 * simple layouts like the Notifier (DesQ Utils) layout or more complex ones like, DesQ Files.
 * Note: It is necessary to set a createWidget functrion before calling generateLayout(...).
 */
class DesQUI::LayoutManager {
    public:
        LayoutManager( QSize referenceSize );
        ~LayoutManager();

        void setWidgetCreator( DesQUI::WidgetFactory *factory );

        QBoxLayout * generateLayout( QVariantMap layoutMap );
        QBoxLayout * generateLayout( QString layoutFile );

    private:
        /** Screen size */
        QSize mRefSize;

        /** Parsed layout file */
        QVariantMap mLayout;
        QVariantMap mProperties;

        DesQUI::WidgetFactory *mFactory = nullptr;

        /** Parse the layout */
        void readLayout( QVariantMap layout );

        /** Initialize a row and it's widgets */
        QWidget * addRow( QVariantList row, QString name, QVariantMap props );

        /** Initialize a column and it's widgets */
        QWidget * addColumn( QVariantList row, QString name, QVariantMap props );
};
