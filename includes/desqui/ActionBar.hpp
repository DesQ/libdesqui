/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <desqui/UI.hpp>

namespace DesQUI {
    class ActionBar;
    class ActionBarItem;
}

struct ActionBar_Page_t;
typedef struct ActionBar_Page_t ActionBarPage;

class Expander;

class DesQUI::ActionBar : public QWidget {
    Q_OBJECT;

    public:
        ActionBar( QWidget *, Qt::Orientation orient = Qt::Horizontal );
        ~ActionBar();

        /**
         * By default, the action bar will be fixed.
         * Use this function to make it floating.
         */
        void setFixed( bool );

        /** Add a page to the action bar */
        int addPage();

        /** Return the number of pages in the action bar */
        int pageCount();

        /**
         * Add an action with name @name, icon @icon to page at @page.
         * Returns the id of the action on success, -1 on failure.
         * Page with number @page should exist.
         * @target - Name returned by the signal when clicked
         * @tooltip - Tooltip shown when hovered
         * @checkable - If this action is checkable.
         * @checked - Initial check state of the action
         * Note: If the action is checkable, the toggling of the highlight is automatically performed.
         */
        int addAction( int page, QString name, QIcon icon, QString target, QString tooltip, bool checkable, bool checked );

        /**
         * Set the icon of action with id @item on page @page.
         */
        void setIcon( int page, int item, QIcon icon );

        /* Enable/disable an item */
        void setItemEnabled( int page, int item );
        void setItemDisabled( int page, int item );

        /** Add a small space with width/height = 0.5 * ActionBarHeight */
        void addSpace( int page );

        /** Add a large space with width/height = 3 * ActionBarHeight */
        void addStretch( int page );

        /** Add a separator on page @page */
        void addSeparator( int page );

        /** Switch to page @page */
        void switchToPage( int page );

    private:
        void createUI();

        /** ActionBar expander */
        Expander *expander = nullptr;

        /** Stack that shows the correct page */
        QStackedWidget *stack = nullptr;

        /** ActionBar's pages */
        QList<ActionBarPage *> pages;

        /** Orientation of the ActionBar */
        Qt::Orientation mOrient;

    protected:
        void leaveEvent( QEvent * );

    Q_SIGNALS:
        void action( QString );
};

class DesQUI::ActionBarItem : public QToolButton {
    Q_OBJECT;

    public:
        ActionBarItem( QString name, QIcon icon, QString target, QString tooltip, bool checkable, bool checked, QWidget *parent );

        void setEnabled();
        void setDisabled();

        void setIcon( QIcon );

    protected:
        void paintEvent( QPaintEvent *pEvent );

    Q_SIGNALS:
        void triggered( QString );

        void switchToPage( int );
};
