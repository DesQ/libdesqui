/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QLayout>
#include <QRect>
#include <QStyle>
#include <QWidgetItem>
#include <QWidget>

typedef QList<QLayoutItem *> QLayoutItemList;

namespace DesQUI {
    class FlexiLayout;
}

class DesQUI::FlexiLayout : public QLayout {
    public:
        enum LayoutMode {
            LeftAligned  = 0xFFF900,            // Left-align the contents -> Pad the extra space after
            Centered     = 0xFFF901,            // Center the contents -> Pad the extra space before and
                                                // after
            RightAligned = 0xFFF902,            // Right-Align the contents -> Pad the extra space before
            Stretched    = 0xFFF903,            // Stretch the content -> Use the extra space to stretch the
                                                // widgets which can be stretched
        };

        FlexiLayout( QWidget *parent );
        FlexiLayout();
        ~FlexiLayout();

        void addItem( QLayoutItem *item );
        void clear();

        int horizontalSpacing() const;
        void setHorizontalSpacing( int );

        int verticalSpacing() const;
        void setVerticalSpacing( int );

        QMargins contentsMargins() const;
        void setContentsMargins( QMargins );

        LayoutMode layoutMode() const;
        void setLayoutMode( LayoutMode mode );

        Qt::Orientations expandingDirections() const;
        bool hasHeightForWidth() const;
        int heightForWidth( int ) const;

        int count() const;

        QLayoutItem *itemAt( int index ) const;
        QLayoutItem *takeAt( int index );

        QSize minimumSize() const;

        QRect geometry() const;
        void setGeometry( const QRect& rect );
        QSize sizeHint() const;

    private:
        int doLayout( const QRect& rect, bool testOnly ) const;
        int smartSpacing( QStyle::PixelMetric pm ) const;

        QList<QLayoutItem *> itemList;
        mutable QMap<int, QLayoutItemList> layoutMap;

        int m_hSpace;
        int m_vSpace;

        int mLayoutMode;

        mutable QRect mGeometry;
};
