/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once
#include <QtWidgets>

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    #define QMouseEnterEvent    QEvent
#else
    #define QMouseEnterEvent    QEnterEvent
#endif

namespace DesQUI {
    class Switch;
}

class DesQUI::Switch : public QAbstractButton {
    Q_OBJECT

    public:
        Switch( QWidget *parent = nullptr );
        ~Switch();

        QSize sizeHint() const override;

    protected:
        void paintEvent( QPaintEvent * ) override;
        void enterEvent( QMouseEnterEvent * ) override;

    private:
        bool _switch = false;
        QSize mSize = QSize( 32, 32 );
        int mThumbPos = 1;

        qreal _opacity = 0.0;

        QVariantAnimation *mAnim = nullptr;
};
