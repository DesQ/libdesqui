/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <desqui/UI.hpp>
#include <desqui/CircularProgress.hpp>

/**
 * DesQUI::UsageGauge class
 *
 * DesQUI::UsageGauge is designed to show the current DesQUsage of various system resources
 * like CPU, Network etc. It has a speedometer like design.
 *
 * A single DesQUI::UsageGauge can show data in multiple segments: 4 segments for quad
 * core CPU, two segments for Network Up and Down, and so on
 */

typedef QList<qint64> GaugeResourceValues;

namespace DesQUI {
    class CircularProgress;
    class UsageGauge;
}

class DesQUI::UsageGauge : public DesQUI::CircularProgress {
    Q_OBJECT;

    public:
        UsageGauge( QString resource, int segments, QWidget *parent );

        /* Maximum value */
        void setMaximum( qint64 max );

        /* Units */
        void setUnits( DesQUI::UsageGauge::Units units );

        /* Labels for the legends */
        void setLabels( QStringList );

        /* Set the bar value */
        void setValues( GaugeResourceValues );

    private:
        int mSegments;
        DesQUI::UsageGauge::Units mUnits;
        qint64 sizeFormatHint;
        QString resName;

        qint64 mMax;
        qint64 minWidth;

        GaugeResourceValues mValues;
        QStringList mLabels;

        int labelHeight = 0;

    protected:
        void paintEvent( QPaintEvent * );
};
