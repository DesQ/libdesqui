# libdesqui

The DesQ library for common UI elements to support multiple device form factors.

### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/libdesqui.git libdesqui`
- Enter the `libdesqui` folder
  * `cd libdesqui`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* libdesq (https://gitlab.com/DesQ/libdesq)


### To Do
* Add support for window being moved from one screen to another
  * `QGuiApplication::primaryScreenChanged` seems to be the signal that tells us if the screen of the widget is changed
  * Otherwise we should listen to all signals related to screens, and then use `QDesktopWidget::screenNumber( const QWidget * )`
  * If not, we might have to dig through X11/Wayland
  * Once we have the screen on which our window resides
* Improve the UI backend so that we can dynamically change the UI if the application is moved from one screen to another


### Upcoming
* Any other feature you request for... :)
