/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QFont>
#include <QSize>
#include <QScreen>
#include <QString>
#include <QVariant>
#include <QBoxLayout>

/** Check if the Hjson value is numeric */
bool isInteger( QVariant val );

/** Check if the Hjson value is numeric */
bool isReal( QVariant val );

/** Check if the Hjson value is numeric */
bool isNumeric( QVariant val );

/** Convert QVariant to QMargins */
QMargins getMargins( QVariant margins, QSize screenSize );

/** Convert QVariant to int */
int getSpacing( QVariant spacing, QSize screenSize );

/** Convert two-char alignment value to Qt::Alignment */
Qt::Alignment getAlignment( QVariant obj );

/** Convert the color specs to QColors */
QColor getColor( QVariant clr );

/** Convert QVariant to width */
int getWidth( QVariant obj, QSize screenSize );

/** Convert QVariant to height */
int getHeight( QVariant obj, QSize screenSize );

/** Get the widget type */
QString getType( QVariant type );

/** Get the font weigth from string/number */
QFont::Weight getFontWeight( QVariant weight );

/** Check if the layout has any valid widgets */
bool hasWidgets( QBoxLayout *, QVariantList );
