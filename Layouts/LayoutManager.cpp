/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtCore>
#include <DFHjsonParser.hpp>

#include "LayoutManager.hpp"
#include "LayoutUtils.hpp"

DesQUI::LayoutManager::LayoutManager( QSize referenceSize ) {
    mRefSize = referenceSize;
}


void DesQUI::LayoutManager::setWidgetCreator( DesQUI::WidgetFactory *factory ) {
    if ( mFactory != nullptr ) {
        delete mFactory;
    }

    mFactory = factory;
}


QBoxLayout * DesQUI::LayoutManager::generateLayout( QString lytFile ) {
    QVariantMap layout = DFL::Config::Hjson::readConfigFromFile( lytFile );

    return generateLayout( layout );
}


QBoxLayout * DesQUI::LayoutManager::generateLayout( QVariantMap layout ) {
    mLayout     = layout[ "Layout" ].toMap();
    mProperties = layout[ "Properties" ].toMap();

    QVariantList layoutItems = mLayout[ "Keys" ].toList();

    /** Do we have a row? If yes, we will be expanding the size vertically */
    QRegularExpression rows = QRegularExpression( "^R[0-9]+$" );

    /** Do we have a column? If yes, we will be expanding the size horizontally */
    QRegularExpression cols = QRegularExpression( "^C[0-9]+$" );

    QBoxLayout *topLyt;

    /** Vertical Layout */
    if ( mLayout[ "Direction" ].toString() == "Vertical" ) {
        topLyt = new QBoxLayout( QBoxLayout::TopToBottom );
    }

    else {
        topLyt = new QBoxLayout( QBoxLayout::LeftToRight );
    }

    if ( mFactory == nullptr ) {
        qCritical() << "Please call setWidgetCreator(...) before generating a layout";
        return topLyt;
    }

    topLyt->setContentsMargins( getMargins( mLayout[ "Margins" ], mRefSize ) );
    topLyt->setSpacing( getSpacing( mLayout[ "Spacing" ], mRefSize ) );

    topLyt->setGeometry( QRect( QPoint( 0, 0 ), mRefSize ) );

    for ( int idx = 0; idx < (int)layoutItems.size(); ++idx ) {
        QString key( layoutItems[ idx ].toString() );

        /** Vertical Layout: Each key will lead to new rows */
        if ( rows.match( key ).hasMatch() ) {
            QVariantList row = mLayout[ key ].toList();
            QWidget      *w  = addRow( row, key, mProperties[ key ].toMap() );

            if ( w != nullptr ) {
                topLyt->addWidget( w );
            }
        }

        /** Horizontal Layout: Each key will lead to new columns */
        else if ( cols.match( key ).hasMatch() ) {
            QVariantList col = mLayout[ key ].toList();
            QWidget      *w  = addColumn( col, key, mProperties[ key ].toMap() );

            if ( w != nullptr ) {
                topLyt->addWidget( w );
            }
        }

        /** Should not have come here */
        else {
            return topLyt;
        }
    }

    return topLyt;
}


QWidget * DesQUI::LayoutManager::addRow( QVariantList row, QString name, QVariantMap props ) {
    if ( props.contains( "Enabled" ) && (props[ "Enabled" ].toBool() == false) ) {
        return nullptr;
    }

    /** Do we have a row? If yes, we will be expanding the size vertically */
    QRegularExpression rows = QRegularExpression( "^R[0-9]+$" );
    /** Do we have a column? If yes, we will be expanding the size horizontally */
    QRegularExpression cols = QRegularExpression( "^C[0-9]+$" );

    QBoxLayout *lyt = new QBoxLayout( QBoxLayout::LeftToRight );

    lyt->setContentsMargins( getMargins( props[ "Margins" ], mRefSize ) );
    lyt->setSpacing( getSpacing( props[ "Spacing" ], mRefSize ) );

    double height = getHeight( props[ "Height" ], mRefSize );

    if ( height ) {
        if ( height < 1.0 ) {
            lyt->addStrut( height * mRefSize.height() );
        }

        else {
            lyt->addStrut( (int)height );
        }
    }

    for ( int idx = 0; idx < (int)row.size(); ++idx ) {
        QString     key( row[ idx ].toString() );
        QVariantMap properties = mProperties[ key ].toMap();

        /** Vertical Layout: Each key will leat to new rows */
        if ( rows.match( key ).hasMatch() ) {
            QVariantList row = mLayout[ key ].toList();
            QWidget      *w  = addRow( row, key, properties );

            if ( w != nullptr ) {
                lyt->addWidget( w );
            }
        }

        /** Horizontal Layout: Each key will leat to new columns */
        else if ( cols.match( key ).hasMatch() ) {
            QVariantList col = mLayout[ key ].toList();
            QWidget      *w  = addColumn( col, key, properties );

            if ( w != nullptr ) {
                lyt->addWidget( w );
            }
        }

        /** Initialize the widget */
        else {
            if ( key == "Stretch" ) {
                lyt->addStretch();
                continue;
            }

            /** We can have multiple widgets with same name (ex: Clock(Time), Clock(Date)) */
            QString widget = key.split( "-" ).at( 0 );

            QWidget *w = mFactory->createWidget( widget, getType( properties[ "Type" ] ), properties );

            if ( w != nullptr ) {
                lyt->addWidget( w, 0, getAlignment( props[ "Alignment" ] ) );
            }
        }
    }

    if ( hasWidgets( lyt, row ) ) {
        Container *cw = new Container( props, name );

        cw->setLayout( lyt );

        return cw;
    }

    return nullptr;
}


QWidget * DesQUI::LayoutManager::addColumn( QVariantList col, QString name, QVariantMap props ) {
    if ( props.contains( "Enabled" ) && (props[ "Enabled" ].toBool() == false) ) {
        return nullptr;
    }

    /** Do we have a row? If yes, we will be expanding the size vertically */
    QRegularExpression rows = QRegularExpression( "^R[0-9]+$" );
    /** Do we have a column? If yes, we will be expanding the size horizontally */
    QRegularExpression cols = QRegularExpression( "^C[0-9]+$" );

    QBoxLayout *lyt = new QBoxLayout( QBoxLayout::TopToBottom );

    lyt->setContentsMargins( getMargins( props[ "Margins" ], mRefSize ) );
    lyt->setSpacing( getSpacing( props[ "Spacing" ], mRefSize ) );

    double width = getWidth( props[ "Width" ], mRefSize );

    lyt->addStrut( (int)width );

    for ( int idx = 0; idx < (int)col.size(); ++idx ) {
        QString     key( col[ idx ].toString() );
        QVariantMap properties = mProperties[ key ].toMap();

        /** Vertical Layout: Each key will leat to new rows */
        if ( rows.match( key ).hasMatch() ) {
            QVariantList row = mLayout[ key ].toList();
            QWidget      *w  = addRow( row, key, properties );

            if ( w != nullptr ) {
                lyt->addWidget( w );
            }
        }

        /** Horizontal Layout: Each key will leat to new columns */
        else if ( cols.match( key ).hasMatch() ) {
            QVariantList col = mLayout[ key ].toList();
            QWidget      *w  = addColumn( col, key, properties );

            if ( w != nullptr ) {
                lyt->addWidget( w );
            }
        }

        /** Initialize the widget */
        else {
            if ( key == "Stretch" ) {
                lyt->addStretch();
                continue;
            }

            QString widget = key.split( "-" ).at( 0 );

            QWidget *w = mFactory->createWidget( widget, getType( properties[ "Type" ] ), properties );

            if ( w != nullptr ) {
                lyt->addWidget( w, 0, getAlignment( props[ "Alignment" ] ) );
            }
        }
    }

    if ( hasWidgets( lyt, col ) ) {
        Container *cw = new Container( props, name );

        cw->setLayout( lyt );

        return cw;
    }

    return nullptr;
}


DesQUI::Container::Container( QVariantMap props, QString name ) : QWidget() {
    /** Object Name */
    setObjectName( name );

    bgColor      = getColor( props[ "BGColor" ] );
    borderColor  = getColor( props[ "BorderColor" ] );
    borderRadius = props[ "BorderRadius" ].toDouble();
}


void DesQUI::Container::paintEvent( QPaintEvent *pEvent ) {
    if ( bgColor != QColor( Qt::transparent ) ) {
        QPainter painter( this );
        painter.setRenderHints( QPainter::Antialiasing );

        painter.setBrush( bgColor );

        if ( borderColor.alpha() == 0 ) {
            painter.setPen( Qt::NoPen );
        }

        else {
            painter.setPen( QPen( borderColor, 1.0 ) );
        }

        painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), borderRadius, borderRadius );

        painter.end();
    }

    QWidget::paintEvent( pEvent );
}


DesQUI::WidgetFactory::WidgetFactory( QSize scrSize ) {
    mRefSize = scrSize;
}


QWidget * DesQUI::WidgetFactory::createWidget( QString, QString, QVariantMap ) {
    return new QWidget();
}


QStringList DesQUI::WidgetFactory::availableWidgets() {
    return QStringList() << "QWidget";
}


QStringList DesQUI::WidgetFactory::availableTypes( QString ) {
    return QStringList();
}


QStringList DesQUI::WidgetFactory::availableProperties( QString, QString ) {
    return QStringList();
}
