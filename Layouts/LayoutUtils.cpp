/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtCore>
#include <QtGui>

#include "LayoutUtils.hpp"

bool isInteger( QVariant val ) {
    switch ( val.userType() ) {
        case QMetaType::UInt:
        case QMetaType::Long:
        case QMetaType::LongLong:
        case QMetaType::ULong:
        case QMetaType::ULongLong:
        case QMetaType::Int: {
            return true;
        }

        default: {
            return false;
        }
    }

    return false;
}


bool isReal( QVariant val ) {
    switch ( val.userType() ) {
        case QMetaType::Float:
        case QMetaType::Double: {
            return true;
        }

        default: {
            return false;
        }
    }

    return false;
}


bool isNumeric( QVariant val ) {
    return isInteger( val ) or isReal( val );
}


QMargins getMargins( QVariant margins, QSize screenSize ) {
    if ( margins.isNull() ) {
        return QMargins();
    }

    double left   = 0;
    double right  = 0;
    double top    = 0;
    double bottom = 0;

    if ( isNumeric( margins ) ) {
        left = right = top = bottom = margins.toDouble();
    }

    else if ( margins.userType() == QMetaType::QVariantList ) {
        QVariantList marginList = margins.toList();

        if ( marginList.length() == 0 ) {
            return QMargins();
        }

        if ( marginList.length() == 4 ) {
            left   = marginList[ 0 ].toDouble();
            right  = marginList[ 1 ].toDouble();
            top    = marginList[ 2 ].toDouble();
            bottom = marginList[ 3 ].toDouble();
        }

        else if ( marginList.length() == 2 ) {
            left = right = marginList[ 0 ].toDouble();
            top  = bottom = marginList[ 1 ].toDouble();
        }

        else {
            left = right = top = bottom = marginList[ 0 ].toDouble();
        }
    }

    else {
        QVariantMap marginMap = margins.toMap();

        if ( isNumeric( marginMap[ "Left" ] ) ) {
            left = marginMap[ "Left" ].toDouble();
        }

        if ( isNumeric( marginMap[ "Right" ] ) ) {
            right = marginMap[ "Right" ].toDouble();
        }

        if ( isNumeric( marginMap[ "Top" ] ) ) {
            top = marginMap[ "Top" ].toDouble();
        }

        if ( isNumeric( marginMap[ "Bottom" ] ) ) {
            bottom = marginMap[ "Bottom" ].toDouble();
        }
    }

    if ( left < 1.0 ) {
        left *= screenSize.width();
    }

    if ( right < 1.0 ) {
        right *= screenSize.width();
    }

    if ( top < 1.0 ) {
        top *= screenSize.height();
    }

    if ( bottom < 1.0 ) {
        bottom *= screenSize.height();
    }

    return QMargins( left, top, right, bottom );
}


int getSpacing( QVariant value, QSize ) {
    if ( value.isNull() ) {
        return 0;
    }

    if ( isInteger( value ) ) {
        return value.toInt();
    }

    return 0;
}


Qt::Alignment getAlignment( QVariant value ) {
    QString       align( value.toString() );
    Qt::Alignment XX;

    /** We'll manage the alignment externally */
    if ( align.isEmpty() ) {
        return XX;
    }

    if ( align.at( 0 ) == QChar( 'T' ) ) {
        XX = Qt::AlignTop;
    }

    else if ( align.at( 0 ) == QChar( 'M' ) ) {
        XX = Qt::AlignVCenter;
    }

    else if ( align.at( 0 ) == QChar( 'B' ) ) {
        XX = Qt::AlignBottom;
    }

    if ( align.length() == 2 ) {
        if ( align.at( 1 ) == QChar( 'L' ) ) {
            XX |= Qt::AlignLeft;
        }

        else if ( align.at( 1 ) == QChar( 'M' ) ) {
            XX |= Qt::AlignHCenter;
        }

        else if ( align.at( 1 ) == QChar( 'R' ) ) {
            XX |= Qt::AlignRight;
        }
    }

    else {
        XX |= Qt::AlignLeft;
    }

    return XX;
}


QColor getColor( QVariant clr ) {
    /** Either 0xAARRGGBB format, or #AARRGGBB format */
    if ( clr.userType() == QMetaType::QString ) {
        QString clrRaw = clr.toString();

        if ( clrRaw.startsWith( "#" ) ) {
            return QColor( clrRaw );
        }

        else if ( clrRaw.startsWith( "0x" ) ) {
            return QColor( clrRaw.replace( "0x", "#" ) );
        }

        return QColor( Qt::transparent );
    }

    if ( clr.userType() == QMetaType::QVariantList ) {
        QVariantList bgClr = clr.toList();

        int red   = (bgClr[ 0 ].userType() == QMetaType::Double ? round( bgClr[ 0 ].toDouble() * 255 ) : bgClr[ 0 ].toInt() );
        int green = (bgClr[ 1 ].userType() == QMetaType::Double ? round( bgClr[ 1 ].toDouble() * 255 ) : bgClr[ 1 ].toInt() );
        int blue  = (bgClr[ 2 ].userType() == QMetaType::Double ? round( bgClr[ 2 ].toDouble() * 255 ) : bgClr[ 2 ].toInt() );
        int alpha = (bgClr[ 3 ].userType() == QMetaType::Double ? round( bgClr[ 3 ].toDouble() * 255 ) : bgClr[ 3 ].toInt() );

        return QColor( red, green, blue, alpha );
    }

    if ( clr.userType() == QMetaType::QVariantMap ) {
        QVariantMap bgClr = clr.toMap();

        int red   = (bgClr[ "Red" ].userType() == QMetaType::Double ? round( bgClr[ "Red" ].toDouble() * 255 )   : bgClr[ "Red" ].toInt() );
        int green = (bgClr[ "Green" ].userType() == QMetaType::Double ? round( bgClr[ "Green" ].toDouble() * 255 ) : bgClr[ "Green" ].toInt() );
        int blue  = (bgClr[ "Blue" ].userType() == QMetaType::Double ? round( bgClr[ "Blue" ].toDouble() * 255 )  : bgClr[ "Blue" ].toInt() );
        int alpha = (bgClr[ "Alpha" ].userType() == QMetaType::Double ? round( bgClr[ "Alpha" ].toDouble() * 255 ) : bgClr[ "Alpha" ].toInt() );

        return QColor( red, green, blue, alpha );
    }

    return QColor( Qt::transparent );
}


int getWidth( QVariant obj, QSize screenSize ) {
    double width = obj.toDouble();

    if ( width <= 1.0 ) {
        width *= screenSize.width();
    }

    if ( width < 0 ) {
        width = 0;
    }

    return (int)width;
}


int getHeight( QVariant obj, QSize screenSize ) {
    double height = obj.toDouble();

    if ( height <= 1.0 ) {
        height *= screenSize.height();
    }

    if ( height < 0 ) {
        height = 0;
    }

    return (int)height;
}


QString getType( QVariant type ) {
    return type.toString();
}


QFont::Weight getFontWeight( QVariant weight ) {
    if ( weight.userType() == QMetaType::QString ) {
        QString w( weight.toString() );

        if ( w == "Thin" ) {
            return QFont::Thin;
        }

        else if ( w == "ExtraLight" ) {
            return QFont::ExtraLight;
        }

        else if ( w == "Light" ) {
            return QFont::Light;
        }

        else if ( w == "Normal" ) {
            return QFont::Normal;
        }

        else if ( w == "Medium" ) {
            return QFont::Medium;
        }

        else if ( w == "DemiBold" ) {
            return QFont::DemiBold;
        }

        else if ( w == "Bold" ) {
            return QFont::Bold;
        }

        else if ( w == "ExtraBold" ) {
            return QFont::ExtraBold;
        }

        else if ( w == "Black" ) {
            return QFont::Black;
        }

        else {
            return QFont::Normal;
        }
    }

    else if ( weight.userType() == QMetaType::Int ) {
        int w( weight.toInt() );

        if ( w >= 87 ) {
            return QFont::Black;
        }

        else if ( w >= 81 ) {
            return QFont::ExtraBold;
        }

        else if ( w >= 75 ) {
            return QFont::Bold;
        }

        else if ( w >= 63 ) {
            return QFont::DemiBold;
        }

        else if ( w >= 57 ) {
            return QFont::Medium;
        }

        else if ( w >= 50 ) {
            return QFont::Normal;
        }

        else if ( w >= 25 ) {
            return QFont::Light;
        }

        else if ( w >= 12 ) {
            return QFont::ExtraLight;
        }

        else {
            return QFont::Thin;
        }
    }

    return QFont::Normal;
}


bool hasWidgets( QBoxLayout *lyt, QVariantList items ) {
    /** All the items have been added to the layout */
    if ( lyt->count() == items.count() ) {
        return true;
    }

    /** Not all items have been added, but may contain widgets */
    else {
        for ( int i = 0; i < lyt->count(); i++ ) {
            /** There is an actual widget */
            if ( lyt->itemAt( i )->widget() ) {
                return true;
            }

            /** Only spacers */
            else {
                //
            }
        }

        /** Contains only spacers without widgets or layouts */
        return false;
    }

    /** Should not have come here */
    return false;
}
