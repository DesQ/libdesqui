/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desqui/UI.hpp>
#include "ActionButton.hpp"

DesQUI::ActionButton::ActionButton( QString name, QIcon icon, QString target ) : QWidget() {
    /** Main Action Button */
    mMainBtn    = new QToolButton( this );
    mMainTarget = target;

    mMainBtn->setIcon( icon );
    mMainBtn->setAcceptDrops( false );

    mMainBtn->setIconSize( QSize( 48, 48 ) );
    mMainBtn->setFixedSize( QSize( 56, 56 ) );

    mMainBtn->setAutoRaise( true );

    QPalette btnPlt = palette();

    btnPlt.setColor( QPalette::Button, QColor( Qt::transparent ) );
    mMainBtn->setPalette( btnPlt );

    mMainBtn->setToolTip( name );
    QHBoxLayout *btnLyt = new QHBoxLayout();

    btnLyt->addStretch();
    btnLyt->addWidget( mMainBtn );

    mLyt = new QFormLayout();

    mLyt->setContentsMargins( QMargins( 4, 4, 4, 4 ) );
    mLyt->setSpacing( 2 );
    mLyt->setAlignment( Qt::AlignCenter );

    QVBoxLayout *baseLyt = new QVBoxLayout();

    baseLyt->setContentsMargins( QMargins() );
    baseLyt->setSpacing( 0 );

    baseLyt->addLayout( mLyt );
    baseLyt->addLayout( btnLyt );

    setLayout( baseLyt );

    connect( mMainBtn, &QToolButton::clicked, this, &DesQUI::ActionButton::handleClick );
}


void DesQUI::ActionButton::addAction( QString name, QIcon icon, QString context, QString target ) {
    if ( not mContextActionMap.contains( context ) ) {
        mContextActionMap[ context ] = ActionList();
    }

    Action action{ name, icon, target };

    mContextActionMap[ context ] << action;
}


void DesQUI::ActionButton::setContexts( QString contexts ) {
    /** Save the contexts */
    mContexts = contexts.split( ";" );
}


void DesQUI::ActionButton::handleClick() {
    /**
     * If the actionList is populated => actions are shown. Hide them.
     */
    if ( actionBtns.count() ) {
        reset();
        return;
    }

    /**
     * If @mMainTarget is not empty and no actions are set, emit triggered( mMainTarget );
     */
    if ( not mMainTarget.isEmpty() and not mContextActionMap.count() ) {
        emit triggered( mMainTarget );
        return;
    }

    /**
     * If @mMainTarget is not empty and no context is set, emit triggered( mMainTarget );
     */
    if ( not mMainTarget.isEmpty() and not mContexts.count() ) {
        emit triggered( mMainTarget );
        return;
    }

    /**
     * If a context is set:
     * Try to create the actions to show it to the user.
     * If actions are available, show them.
     * Else, emit triggered( mMainTarget )
     */
    if ( mContexts.count() ) {
        /** Clear up old crud */
        reset();

        /** Create new button */
        for ( QString ctxt: mContexts ) {
            ActionList actions = mContextActionMap[ ctxt ];
            for ( Action action: actions ) {
                QToolButton *btn = new QToolButton();
                btn->setIcon( action.icon );
                btn->setIconSize( QSize( 24, 24 ) );
                btn->setFixedSize( QSize( 32, 32 ) );
                btn->setAutoRaise( true );

                QLabel *lbl = new QLabel( action.name );

                connect(
                    btn, &QToolButton::clicked, [ = ] () {
                        emit triggered( action.target );
                        reset();
                    }
                );

                actionBtns << btn;
                actionLbls << lbl;
            }
        }

        /** Add the buttons and show them */
        if ( actionBtns.count() ) {
            for ( int i = 0; i < actionBtns.count(); i++ ) {
                QToolButton *btn = actionBtns.at( i );
                QLabel      *lbl = actionLbls.at( i );

                mLyt->addRow( lbl, btn );

                qApp->processEvents();
            }
        }

        else {
            if ( not mMainTarget.isEmpty() ) {
                emit triggered( mMainTarget );
            }
        }
    }
}


void DesQUI::ActionButton::reset() {
    while ( actionBtns.count() ) {
        QToolButton *btn = actionBtns.takeFirst();
        QLabel      *lbl = actionLbls.takeFirst();

        mLyt->removeWidget( lbl );
        mLyt->removeWidget( btn );

        delete lbl;
        delete btn;

        qApp->processEvents();
    }

    actionBtns.clear();
}
