/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "ActionBar.hpp"
#include "ActionBarImpl.hpp"

int DesQUI::ActionBarHeight = 36;

DesQUI::ActionBar::ActionBar( QWidget *parent, Qt::Orientation orient ) : QWidget( parent ) {
    mOrient = orient;
    createUI();

    setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed ) );

    /** By default show the toolbar */
    setFixed( true );
}


DesQUI::ActionBar::~ActionBar() {
    if ( expander ) {
        delete expander;
    }

    if ( stack ) {
        delete stack;
    }

    do {
        if ( not pages.count() ) {
            break;
        }

        ActionBarPage *page = pages.takeFirst();
        delete page;
    } while ( true );
}


void DesQUI::ActionBar::setFixed( bool yes ) {
    if ( yes ) {
        if ( not stack->isVisible() ) {
            expander->click();
        }

        qApp->processEvents();

        expander->setDisabled( true );
        expander->hide();
    }

    else {
        expander->setEnabled( true );
    }
}


void DesQUI::ActionBar::createUI() {
    expander = new Expander( mOrient, this );
    stack    = new QStackedWidget();

    stack->setStyleSheet( "QStackedWidget{ border: 1px solid palette(Highlight); border-radius: 3px; background: palette(Window); }" );
    stack->setContentsMargins( QMargins( 1, 1, 1, 1 ) );

    if ( mOrient == Qt::Horizontal ) {
        setFixedHeight( ActionBarHeight + 2 );
        setMinimumWidth( ActionBarHeight + 2 );
    }

    else {
        setFixedWidth( ActionBarHeight + 2 );
        setMinimumHeight( ActionBarHeight + 2 );
    }

    connect(
        expander, &Expander::clicked, [ = ] () {
            if ( stack->isVisible() ) {
                if ( mOrient == Qt::Horizontal ) {
                    setFixedWidth( ActionBarHeight + 2 );
                }

                else {
                    setFixedHeight( ActionBarHeight + 2 );
                }

                stack->hide();
            }

            else {
                if ( not stack->count() ) {
                    return;
                }

                if ( mOrient == Qt::Horizontal ) {
                    stack->setFixedWidth( pages.at( stack->currentIndex() )->pageSize().width() );
                    setFixedWidth( pages.at( stack->currentIndex() )->pageSize().width() + 5 + ActionBarHeight + 2 );
                }

                else {
                    stack->setFixedHeight( pages.at( stack->currentIndex() )->pageSize().height() );
                    setFixedHeight( pages.at( stack->currentIndex() )->pageSize().height() + 5 + ActionBarHeight + 2 );
                }

                stack->show();
            }

            expander->setExpanded( stack->isVisible() );
        }
    );

    setContentsMargins( QMargins() );

    QBoxLayout *wLyt;

    if ( mOrient == Qt::Horizontal ) {
        wLyt = new QBoxLayout( QBoxLayout::LeftToRight );
    }

    else {
        wLyt = new QBoxLayout( QBoxLayout::TopToBottom );
    }

    wLyt->setContentsMargins( QMargins() );
    wLyt->setSpacing( 5 );
    wLyt->addWidget( expander );
    wLyt->addWidget( stack );
    wLyt->addStretch();

    setLayout( wLyt );
}


int DesQUI::ActionBar::addPage() {
    ActionBarPage *page = new ActionBarPage;

    if ( mOrient == Qt::Horizontal ) {
        page->layout = new QBoxLayout( QBoxLayout::LeftToRight );
    }

    else {
        page->layout = new QBoxLayout( QBoxLayout::TopToBottom );
    }

    page->layout->setContentsMargins( QMargins() );
    page->layout->setSpacing( 0 );

    QWidget *pageBase = new QWidget();

    pageBase->setLayout( page->layout );

    pages << page;

    return stack->addWidget( pageBase );
}


int DesQUI::ActionBar::pageCount() {
    return pages.count();
}


int DesQUI::ActionBar::addAction( int page, QString name, QIcon icon, QString target, QString tooltip, bool checkable, bool checked ) {
    if ( pages.count() <= page ) {
        return -1;
    }

    DesQUI::ActionBarItem *item = new DesQUI::ActionBarItem( name, icon, target, tooltip, checkable, checked, this );

    pages.at( page )->layout->addWidget( item );
    pages.at( page )->items << item;

    connect( item, &DesQUI::ActionBarItem::triggered,    this, &DesQUI::ActionBar::action );
    connect( item, &DesQUI::ActionBarItem::switchToPage, this, &DesQUI::ActionBar::switchToPage );

    return pages.at( page )->items.indexOf( item );
}


void DesQUI::ActionBar::setIcon( int page, int item, QIcon icon ) {
    pages.at( page )->items.at( item )->setIcon( icon );
}


void DesQUI::ActionBar::setItemEnabled( int page, int item ) {
    pages.at( page )->items.at( item )->setEnabled();
}


void DesQUI::ActionBar::setItemDisabled( int page, int item ) {
    if ( page >= pages.count() ) {
        qWarning() << "Page" << page << "not found";
        return;
    }

    if ( item >= pages.at( page )->items.count() ) {
        qWarning() << "Item" << item << "not found on page" << page;
        return;
    }

    pages.at( page )->items.at( item )->setDisabled();
}


void DesQUI::ActionBar::addSpace( int page ) {
    if ( pages.count() <= page ) {
        return;
    }

    pages.at( page )->layout->addWidget( new ActionBarSpace( mOrient, this ) );
}


void DesQUI::ActionBar::addStretch( int page ) {
    if ( pages.count() <= page ) {
        return;
    }

    pages.at( page )->layout->addWidget( new ActionBarLongSpace( mOrient, this ) );
}


void DesQUI::ActionBar::addSeparator( int page ) {
    if ( pages.count() <= page ) {
        return;
    }

    pages.at( page )->layout->addWidget( new ActionBarSeparator( mOrient, this ) );
}


void DesQUI::ActionBar::switchToPage( int page ) {
    if ( page >= pages.count() ) {
        return;
    }

    int expanderSize = (expander->isEnabled() ? ActionBarHeight + 5 : 0);

    if ( mOrient == Qt::Horizontal ) {
        stack->setFixedWidth( pages.at( page )->pageSize().width() );
        setFixedWidth( pages.at( page )->pageSize().width() + expanderSize + 2 );
    }

    else {
        stack->setFixedHeight( pages.at( page )->pageSize().height() );
        setFixedHeight( pages.at( page )->pageSize().height() + expanderSize + 2 );
    }

    stack->setCurrentIndex( page );
}


void DesQUI::ActionBar::leaveEvent( QEvent *event ) {
    if ( not stack->isVisible() ) {
        return;
    }

    QTimer::singleShot(
        2000, [ = ]() {
            if ( not underMouse() ) {
                expander->animateClick();
            }
        }
    );

    event->accept();
}


/**
 * DesQUI::ActionBarItem - SidePanelItem class
 **/

DesQUI::ActionBarItem::ActionBarItem( QString name, QIcon icon, QString target, QString tooltip, bool checkable, bool checked, QWidget *parent ) :
    QToolButton( parent ) {
    setFixedSize( QSize( DesQUI::ActionBarHeight, DesQUI::ActionBarHeight ) );

    setAutoRaise( true );

    setText( name );
    setIcon( icon );

    setToolTip( tooltip );

    setCheckable( checkable );
    setChecked( checked );

    setCursor( Qt::PointingHandCursor );
    setAcceptDrops( true );

    connect(
        this, &QToolButton::clicked, [ = ] () {
            QStringList targets = target.split( ";" );
            Q_FOREACH ( QString tgt, targets ) {
                if ( tgt.startsWith( "#" ) ) {
                    emit switchToPage( tgt.replace( "#", "" ).toUInt() );
                }

                else {
                    emit triggered( tgt );
                }
            }
        }
    );

    setStyleSheet( "border: none;" );
}


void DesQUI::ActionBarItem::setIcon( QIcon icon ) {
    QToolButton::setIcon( icon );
}


void DesQUI::ActionBarItem::setEnabled() {
    QWidget::setEnabled( true );
}


void DesQUI::ActionBarItem::setDisabled() {
    QWidget::setDisabled( true );
}


void DesQUI::ActionBarItem::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    painter.setPen( Qt::NoPen );
    QColor highlight( palette().color( QPalette::Highlight ) );

    if ( not isEnabled() ) {
        painter.setBrush( palette().color( QPalette::Window ) );
    }

    else if ( isDown() ) {
        highlight.setAlphaF( 0.6 );
        painter.setBrush( highlight );
    }

    /* Hover and no highlight */
    else if ( underMouse() ) {
        highlight.setAlphaF( 0.3 );
        painter.setBrush( highlight );
    }

    else if ( isChecked() ) {
        highlight.setAlphaF( 0.6 );
        painter.setBrush( highlight.darker( 120 ) );
    }

    /* Clear */
    else {
        painter.setBrush( Qt::transparent );
    }

    painter.drawRect( QRect( 0, 0, width(), height() ) );

    painter.end();

    QToolButton::paintEvent( pEvent );
}
