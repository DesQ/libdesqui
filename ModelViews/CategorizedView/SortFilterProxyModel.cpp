/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This file was originally a part of the KDE project: KF5 ItemViews Module
 * 2007 Rafael Fernández López <ereslibre@kde.org>
 * 2007 John Tapsell <tapsell@kde.org>
 * Original License: LGPL 2.0
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "SortFilterProxyModel.hpp"

#include <QtCore/private/qobject_p.h>
#include <QCollator>

class DesQUI::SortFilterProxyModelPrivate : public QObjectPrivate {
    Q_DECLARE_PUBLIC( DesQUI::SortFilterProxyModel );

    public:
        SortFilterProxyModelPrivate() {
            m_collator.setNumericMode( true );
            m_collator.setCaseSensitivity( Qt::CaseSensitive );
        }

        int sortColumn          = 0;
        Qt::SortOrder sortOrder = Qt::AscendingOrder;
        bool categorizedModel   = false;
        bool sortCategoriesByNaturalComparison = true;
        QCollator m_collator;
};

DesQUI::SortFilterProxyModel::SortFilterProxyModel( QObject *parent ) : QSortFilterProxyModel( parent ) {
}


DesQUI::SortFilterProxyModel::~SortFilterProxyModel() = default;

void DesQUI::SortFilterProxyModel::sort( int column, Qt::SortOrder order ) {
    Q_D( SortFilterProxyModel );

    d->sortColumn = column;
    d->sortOrder  = order;

    QSortFilterProxyModel::sort( column, order );
}


bool DesQUI::SortFilterProxyModel::isCategorizedModel() const {
    Q_D( const SortFilterProxyModel );

    return d->categorizedModel;
}


void DesQUI::SortFilterProxyModel::setCategorizedModel( bool categorizedModel ) {
    Q_D( SortFilterProxyModel );

    if ( categorizedModel == d->categorizedModel ) {
        return;
    }

    d->categorizedModel = categorizedModel;

    invalidate();
}


int DesQUI::SortFilterProxyModel::sortColumn() const {
    Q_D( const SortFilterProxyModel );

    return d->sortColumn;
}


Qt::SortOrder DesQUI::SortFilterProxyModel::sortOrder() const {
    Q_D( const SortFilterProxyModel );

    return d->sortOrder;
}


void DesQUI::SortFilterProxyModel::setSortCategoriesByNaturalComparison( bool sortCategoriesByNaturalComparison ) {
    Q_D( SortFilterProxyModel );

    if ( sortCategoriesByNaturalComparison == d->sortCategoriesByNaturalComparison ) {
        return;
    }

    d->sortCategoriesByNaturalComparison = sortCategoriesByNaturalComparison;

    invalidate();
}


bool DesQUI::SortFilterProxyModel::sortCategoriesByNaturalComparison() const {
    Q_D( const SortFilterProxyModel );

    return d->sortCategoriesByNaturalComparison;
}


bool DesQUI::SortFilterProxyModel::lessThan( const QModelIndex& left, const QModelIndex& right ) const {
    Q_D( const SortFilterProxyModel );

    if ( d->categorizedModel ) {
        int compare = compareCategories( left, right );

        if ( compare > 0 ) { // left is greater than right
            return false;
        }
        else if ( compare < 0 ) { // left is less than right
            return true;
        }
    }

    return subSortLessThan( left, right );
}


bool DesQUI::SortFilterProxyModel::subSortLessThan( const QModelIndex& left, const QModelIndex& right ) const {
    return QSortFilterProxyModel::lessThan( left, right );
}


int DesQUI::SortFilterProxyModel::compareCategories( const QModelIndex& left, const QModelIndex& right ) const {
    Q_D( const SortFilterProxyModel );

    QVariant l = (left.model() ? left.model()->data( left, CategorySortRole ) : QVariant() );
    QVariant r = (right.model() ? right.model()->data( right, CategorySortRole ) : QVariant() );

    Q_ASSERT( l.isValid() );
    Q_ASSERT( r.isValid() );
    Q_ASSERT( l.type() == r.type() );

    if ( l.type() == QVariant::String ) {
        QString lstr = l.toString();
        QString rstr = r.toString();

        if ( d->sortCategoriesByNaturalComparison ) {
            return d->m_collator.compare( lstr, rstr );
        }

        else {
            if ( lstr < rstr ) {
                return -1;
            }

            if ( lstr > rstr ) {
                return 1;
            }

            return 0;
        }
    }

    qlonglong lint = l.toLongLong();
    qlonglong rint = r.toLongLong();

    if ( lint < rint ) {
        return -1;
    }

    if ( lint > rint ) {
        return 1;
    }

    return 0;
}
